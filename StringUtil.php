<?php
/**
 * @author: HeLong
 * @date: 2021-03-24 15:00
 */

namespace ykplug;

class StringUtil
{
    /**
     * 获取字符串长度
     * @param $string
     * @return int
     */
    public static function length($string)
    {
        if (empty($string)) {
            return 0;
        }
        return strlen($string);
    }
}